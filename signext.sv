`timescale 1ns / 1ps

/*
 * COMP300 - Lab 2A: Fetch Datapath
 * University of San Diego
 * 
 * Written by Donghwan Jeon, 4/10/2007
 * Updated by Sat Garcia, 4/8/2008
 *
 *
 * Sign Extender Module
 *
 * parameters:
 * 	IN: data width for the input
 * 	OUT: data width for the output
 */

module signext#(parameter IN=5, OUT=10)
(
    input   [IN-1:0]    d_i,
    output  [OUT-1:0]   d_o
);

// TODO: complete this module

// Don't forget to use the parameters, and don't try using for loops or
// anything crazy!

endmodule
