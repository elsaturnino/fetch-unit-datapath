`timescale 1ns / 1ps

/* 
 * COMP 300 - Lab 2A: Fetch Datapath
 * University of San Diego
 *
 * Written by Donghwan Jeon, 4/10/2007
 * Updated by Sat Garcia, 4/9/2008
 * Updated by Michael Taylor, 4/4/2011
 * Updated by Nikolaos Strikos, 4/11/2012
 */

/* 
 * Fetch unit module
 *
 * parameters:
 *      I_WIDTH: instruction width
 *      A_WIDTH: SRAM address width
 *      O_WIDTH: offset width in a branch instruction (2's complement)
 */

`include "instr_packet_s.sv"

module fetch#(parameter I_WIDTH = 17, A_WIDTH = 10, O_WIDTH = 5)
(
    input   clk,

    // inputs from exec unit
    input   dequeue_i,
    input   restart_i,
    input   [A_WIDTH-1: 0] restart_addr_i,

    // memory interface
    input   load_store_valid_i,
    input   store_en_i,
    input   [A_WIDTH-1: 0] load_store_addr_i,
    input   [I_WIDTH-1: 0] store_data_i,
    output  [I_WIDTH-1: 0] load_data_o,
    output  load_data_valid_o,

    // ouputs to the exec unit
    output  [I_WIDTH-1: 0] instruction_data_o,
    output  [A_WIDTH-1: 0] instruction_addr_o,
    output  instruction_valid_o
);

   logic     fifo_enqueue, fifo_clear, fifo_valid, fifo_full, fifo_empty;
   logic [A_WIDTH-1 : 0] offset;
   logic [A_WIDTH-1 : 0] ram_addr;
   logic [I_WIDTH-1 : 0] ram_data;
   logic                 ram_we;

   logic [A_WIDTH-1 : 0] pc_next;

   // registers for the data path inputs
   logic [A_WIDTH-1 : 0]  load_store_addr_r;
   logic [I_WIDTH-1 : 0]  store_data_r;
   logic [A_WIDTH-1 : 0]  restart_addr_r;

   // validate inputs
   // synthesis translate off
   always_comb
     begin
        if (store_en_r & ~load_store_valid_r)
          $display("Warning: store_en_r set but load_store_valid_r not set!\n");
     end
   // synthesis translate on

   logic [A_WIDTH-1 : 0] pc_r;
   
   // fifo instantiation
   fifo fetch_fifo
     (
      .clk(clk)
      ,.instr_packet_i(ip_in)
      ,.deque_i(dequeue_i)
      ,.clear_i(fifo_clear)
      ,.enque_i(fifo_enqueue)
      ,.instr_packet_o(ip_out)
      ,.empty_o(fifo_empty)
      ,.full_o(fifo_full)
      ,.valid_o(fifo_valid)
      );


   // RAM instantiation
   ram_17_1024b ram
     (
      .address(ram_addr)
      ,.clock(clk)
      ,.data(store_data_r)
      ,.wren(ram_we)
      ,.q(ram_data)             //data_out
      );

   // TODO: noble student, complete this file!
   
endmodule
